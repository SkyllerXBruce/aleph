# Proyecto Aleph Corp.

### Sistema Encuestador 

#### Introducción 
Este es un proyecto con el objetivo de crear, modificar, y obtener encuestas mediante una página WEB.

Este proyecto esta basado en el **Framework Codeigniter 3**, el cual es una aplicación muy poderosa para desarrolladores de PHP que de sean construir una página WEB. Este framework trabaja bajo el modelo de aquitectura MVC, por lo que el programador estará centrado en codificar los detalles de la página a crear, y no en la comunicación ni configuración que se necesita para que su trabajo sea visualizado.

#### Proyecto Aleph

El proyecto dará acceso a 4 tipo de usuarios (administrador del sistema, administrador de estudios, encuestador y analista) los cuales cumplirán con los siguientes roles:

1. **El administrador de sistema**: el cual es el encargado de dar de alta a los usuarios según su rol a desempeñar, adémas el podrá activar, modificar y quitar la manerá en que los administradores de estudio realicen sus cuestionarios.

2. **El administrador de estudios**: el cual es encargado de crear el estudio y cuestionrio a realizar.

3. **Encuestador**: el se encargará de buscar gente para contestar el cuestionario (dependiendo el estudio a realizar) y proporcionará el acceso a estos.

4. **Analista**: el se encargará de proporcionar los resultados de las encuestas realizadas.